#This program goal is to have an agent recognizing people and gestures 
#It uses the Face input, user tracking, hand_gesture input and agent output
import sys
sys.path.append("../")
sys.path.append("../../gen-py")
sys.path.append("../../i2p/tools/py")

import Inputs.FaceService as Face_Service

import Inputs.UserTrackingService as UserTracking_Service
from Inputs.ttypes import *
import Inputs.constants

import EventPublisher.WorldQuery as WorldQuery_Service
from EventPublisher.ttypes import *
import EventPublisher.constants

import Control.AgentControl as AgentControl_Service
import Control.constants
import Control.ttypes

from I2P.ttypes import *

#to ease client server creation in python
import ThriftTools

from numpy import array
import numpy as np
from random import choice

#time
import I2PTime
import transformations

#thread
import threading
import time

#the world model
import worldEntity

import math

user_tracking_device = 'kinect1'

class UserTrackingHandler:
    def __init__(self):
        self.lastGesture = Gesture.STANDING
        self.lastTimestamp = 0
    def userNew(self, sensorID, timestamp, userID):
        print sensorID
        if sensorID == user_tracking_device:
            theWorld.prepareUserNew(userID)
        return
    def userLost(self, sensorID, timestamp, userID):
        if sensorID == user_tracking_device:
            theWorld.prepareUserLost(userID)
        return
    def userSkeletonChanged(self, sensorID, timestamp, userID, position):
        if sensorID == user_tracking_device:
            theWorld.userSkeletonChanged( userID, position )
        return
    def userImageChanged(self, sensorID, timestamp, userID, position):
        return
    def gestureStart(self, sensorID, timestamp, userID, gesture):
        if gesture != self.lastGesture or ((timestamp - self.lastTimestamp) >= 3*1000000):  #4 seconds
            theWorld.prepareRecoGesture(gesture)
            self.lastGesture = gesture
            self.lastTimestamp = timestamp
        return
    def gestureStop(self, sensorID, timestamp, userID, gesture):
        return
        
#the function listening for face inputs        
class FaceRecoHandler:
    def __init__(self):
        return
        
    def __del__(self):
        return
        
    def faceRecognized(self, sensorID, timestamp, name, imagePosition):
        if len(name) == 0:
            # unknown person
            name = 'unknown'
        #react to recognized person
        theWorld.personRecognized(name)    
#the function updating position to look at
class WorldQueryHandler:
    def __init__(self):
        return
        
    def __del__(self):
        return
        
    def getLocation(self, target):
        targetLocation = EventPublisher.ttypes.Location()
        pos = I2P.ttypes.Vec3()
        pos.x = 0.0
        pos.y = 0.0
        pos.z = 0.0
        quat = I2P.ttypes.Vec4()
        quat.x = 0.0
        quat.y = 0.0
        quat.z = 0.0
        quat.w = 1.0
        #print 'look for target ' + target
        try:
            entity = theWorld.worldModel[target]
            #print 'target found'
            entityPosition = entity.getPosition()
            entityOrientation = entity.getOrientation()
            pos.x = entityPosition[0]
            pos.y = entityPosition[1]
            pos.z = entityPosition[2]
            quat.w = entityOrientation[0]
            quat.x = entityOrientation[1]
            quat.y = entityOrientation[2]
            quat.z = entityOrientation[3]
            
            #print pos
            targetLocation.isUnknown = False
        except KeyError:
            #print 'target NOT found'
            targetLocation.isUnknown = True
        targetLocation.location = pos 
        targetLocation.orientation = quat

        return targetLocation

class World(threading.Thread):
    def __init__(self,smartbody_client):
        threading.Thread.__init__(self)
        self.stopping = False
        self.smartbody_client = smartbody_client
        
        self.worldModel = dict()
        #initialize the scene
        display = worldEntity.WorldEntity('VH_Display_01')
        display.setPosition(array([0.6,0.65,1.8]))
        #display.setOrientation(array([1.0,0.0,0.0,0.0]))
        display.setOrientation(transformations.quaternion_about_axis(math.pi/4.0,array([0.0,0.0,1.0])))
        self.worldModel['VH_Display_01'] = display
        
        subject = worldEntity.WorldEntity('Subject')
        subject.setPosition(array([0.0,0.0,0.0]))
        subject.setOrientation(array([1.0,0.0,0.0,0.0]))
        self.worldModel['Subject'] = subject

        userTrack = worldEntity.WorldEntity(user_tracking_device)
        userTrack.setPosition(array([1.5,0.0,1.8]))
        userTrack.setOrientation(transformations.quaternion_about_axis(math.pi/2.0,array([0.0,0.0,1.0])))
        self.worldModel[user_tracking_device] = userTrack
        
        self.currentTarget = 'unknown'
        
        self.gesture = ''
        self.isRecoGesture = False 
        self.isNewUser = False
        self.isLostUser = False
        
    def run(self):
        while(not self.stopping):
            if self.isNewUser:
                self.doUserNew()
            if self.isLostUser:
                self.doUserLost()
            if self.isRecoGesture:
                self.doRecoGesture()
            time.sleep(1)
        self.smartbody_client.disconnect()
        
    def prepareUserNew(self,userID):
        self.isNewUser = True
        
    def prepareUserLost(self,userID):
        self.isLostUser = True

    def doUserNew(self):
        self.smartbody_client.client.lookAtTarget('Subject')
        self.smartbody_client.client.setFaceExpression(I2P.ttypes.Facial_Expression.HAPPINESS,10)
        self.smartbody_client.client.playAnimation(Control.ttypes.Animation.WAVE_HAND)
        self.smartbody_client.client.speak('Hi, I am Chloe.',10)
        time.sleep(5.0)
        self.isNewUser = False
    
    def doUserLost(self):
        self.smartbody_client.client.playAnimation(Control.ttypes.Animation.WAVE_HAND)
        self.smartbody_client.client.setFaceExpression(I2P.ttypes.Facial_Expression.SADNESS,10)
        self.smartbody_client.client.speak('Good bye. I hope to see you again.',10)
        self.smartbody_client.client.endLookAt()
        self.isLostUser = False
        
    def userSkeletonChanged(self, userID, position):
        # sometimes we got a skeleton changed message even before the user new
        self.smartbody_client.client.lookAtTarget('Subject')
        headPos = convertFromOpenGLToWorld(convertFromVec3ToArray(position.head))
        sensor_rotation = transformations.quaternion_matrix( self.worldModel[user_tracking_device].getOrientation() )
        headPos = self.worldModel[user_tracking_device].getPosition() + np.dot(headPos,sensor_rotation[:3,:3].T)
        self.worldModel['Subject'].setPosition(headPos)
        #print ('userSkeletonChanged\n')
 
    def stop(self):
        self.stopping = True
        
    def personRecognized(self, name):
        newPerson = False
        if self.currentTarget != name:
            newPerson = True
            self.currentTarget = name
        if name != 'unknown' and newPerson:
            if name == 'Bertil':
                name = 'Professor Anderson'
            self.smartbody_client.client.speak('Hello, ' + name,10)
        
    def prepareRecoGesture(self, gesture):
        self.gesture = gesture
        self.isRecoGesture = True            
            
    #FOR XIAO YANG
    def doRecoGesture(self):
        if self.gesture == Gesture.RAISINGHAND:
            self.smartbody_client.client.speak('You are raising your hand,',10)
        elif self.gesture == Gesture.WAVINGHAND:
            self.smartbody_client.client.playAnimation(Control.ttypes.Animation.WAVE_HAND)
            self.smartbody_client.client.speak('Hello,',10)
        elif self.gesture == Gesture.HAVINGQUESTION:
            self.smartbody_client.client.setFaceExpression(I2P.ttypes.Facial_Expression.NEUTRAL,10)
            self.smartbody_client.client.playAnimation(Control.ttypes.Animation.POINTING_YOU)
            self.smartbody_client.client.speak('What is your question,',10)
        elif self.gesture == Gesture.OBJECTION:
            self.smartbody_client.client.setFaceExpression(I2P.ttypes.Facial_Expression.SADNESS,10)
            self.smartbody_client.client.playAnimation(Control.ttypes.Animation.RIGHT_SWEEP)
            self.smartbody_client.client.speak('You do not agree,',10)
        elif self.gesture == Gesture.PRAISE:
            self.smartbody_client.client.setFaceExpression(I2P.ttypes.Facial_Expression.HAPPINESS,10)
            self.smartbody_client.client.playAnimation(Control.ttypes.Animation.POINTING_YOU)
            self.smartbody_client.client.speak('You are praising,',10)
        elif self.gesture == Gesture.AGREEMENT:
            self.smartbody_client.client.setFaceExpression(I2P.ttypes.Facial_Expression.HAPPINESS,10)
            self.smartbody_client.client.playAnimation(Control.ttypes.Animation.OFFER)
            self.smartbody_client.client.speak('You agree,',10)
        elif self.gesture == Gesture.CONFIDENT:
            self.smartbody_client.client.setFaceExpression(I2P.ttypes.Facial_Expression.HAPPINESS,10)
            self.smartbody_client.client.playAnimation(Control.ttypes.Animation.PAPER)
            self.smartbody_client.client.speak('You are confident,',10)
        elif self.gesture == Gesture.ONCALL:
            self.smartbody_client.client.setFaceExpression(I2P.ttypes.Facial_Expression.NEUTRAL,10)
            self.smartbody_client.client.playAnimation(Control.ttypes.Animation.HAND_ON_HIP)
            self.smartbody_client.client.speak('You are calling,',10)
        elif self.gesture == Gesture.STOP:
            self.smartbody_client.client.setFaceExpression(I2P.ttypes.Facial_Expression.SURPRISE,10)
            self.smartbody_client.client.playAnimation(Control.ttypes.Animation.WHY)
            self.smartbody_client.client.speak('Why do you stop me,',10)
        elif self.gesture == Gesture.SUCCESS:
            self.smartbody_client.client.setFaceExpression(I2P.ttypes.Facial_Expression.HAPPINESS,10)
            self.smartbody_client.client.playAnimation(Control.ttypes.Animation.PAPER)
            self.smartbody_client.client.speak('You are successful,',10)
        elif self.gesture == Gesture.READ:
            self.smartbody_client.client.setFaceExpression(I2P.ttypes.Facial_Expression.NEUTRAL,10)
            self.smartbody_client.client.playAnimation(Control.ttypes.Animation.OFFER)
            self.smartbody_client.client.speak('You are reading,',10)
        elif self.gesture == Gesture.WRITE:
            self.smartbody_client.client.setFaceExpression(I2P.ttypes.Facial_Expression.NEUTRAL,10)
            self.smartbody_client.client.playAnimation(Control.ttypes.Animation.HAND_ON_HIP)
            self.smartbody_client.client.speak('You are writing,',10)
        elif self.gesture == Gesture.DRINK:
            self.smartbody_client.client.setFaceExpression(I2P.ttypes.Facial_Expression.NEUTRAL,10)
            self.smartbody_client.client.playAnimation(Control.ttypes.Animation.OFFER)
            self.smartbody_client.client.speak('You are drinking,',10)            
        time.sleep(4)
        self.isRecoGesture = False 
    #END FOR XIAO YANG
            
def convertFromVec3ToArray( vec ):
    return array([vec.x, vec.y, vec.z])
    
def convertFromOpenGLToWorld( vec ):
    return array([vec[2],vec[0],vec[1]])
        
def readKeyboard1():
    found = False
    # exit condition
    res = -2
    while not found:
        res = raw_input('Enter s to start virtual human client. \
                         Press enter after the key entry.\n')
        if len(res) != 1:
            print ('Wrong option\n')
        else:
            # test exit
            if res[0] == 's':
                res = -1
                found = True
                continue
    return res
    
    
def readKeyboard2():
    found = False
    # exit condition
    res = -2
    while not found:
        res = raw_input('Enter q to exit. \
                         Press enter after the key entry.\n')
        if len(res) != 1:
            print ('Wrong option\n')
        else:
            # test exit
            if res[0] == 'q':
                res = -1
                found = True
                continue
    return res
    
if __name__ == "__main__":
    #starting user tracking and gesture recognition service
    userTracking_handler = UserTrackingHandler()
    userTracking_server = ThriftTools.ThriftServerThread(Inputs.constants.DEFAULT_USERTRACKING_SERVICE_PORT,\
        UserTracking_Service,userTracking_handler,'User Tracking server','155.69.52.73')
		#UserTracking_Service,userTracking_handler,'User Tracking server','localhost')
    userTracking_server.start()
    
     #starting face service
    faceReco_handler = FaceRecoHandler()
    faceReco_server = ThriftTools.ThriftServerThread(Inputs.constants.DEFAULT_FACE_SERVICE_PORT,Face_Service,faceReco_handler,'Face recognition')
    faceReco_server.start()
    
    #starting worldQuery
    worldQuery_handler = WorldQueryHandler()
    worldQuery_server = ThriftTools.ThriftServerThread(EventPublisher.constants.DEFAULT_WORLD_QUERY_PORT,WorldQuery_Service,worldQuery_handler,'World Query')
    worldQuery_server.start()
    
    #starting worldEvent
    worldEvent_handler = WorldEventHandler()
    worldEvent_server = ThriftTools.ThriftServerThread(EventPublisher.constants.DEFAULT_WORLD_EVENT_PORT,WorldEvent_Service,worldEvent_handler,'World Event')
    worldEvent_server.start()
    
    res = readKeyboard1()
    while res != -1:
        res = readKeyboard1()
        
    smartbody_client = ThriftTools.ThriftClient('localhost',9090,AgentControl_Service,'SmartBody')
    smartbody_client.connect()
    theWorld = World(smartbody_client)
    theWorld.start()
    
    servers = [userTracking_server,worldQuery_server,theWorld]
    
    res = readKeyboard2()
    while res != -1:
        res = readKeyboard2()
    for s in servers:
        s.stop()
    print 'done'