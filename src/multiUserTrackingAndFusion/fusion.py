#This program goal is to server as a fusion and decision making module for multiusertrackingandFusion application
import sys
sys.path.append("../")
sys.path.append("../../gen-py")
sys.path.append("../../i2p/tools/py")

import Inputs.WorldFusionService as WorldFusion_Service
from Inputs.ttypes import *
import Inputs.constants

import EventPublisher.WorldQuery as WorldQuery_Service
from EventPublisher.ttypes import *
import EventPublisher.constants

import Control.FusionControl as FusionControl_Service

import Control.AgentControl as AgentControl_Service
import Control.constants
import Control.ttypes

#This service goal is to have an agent looking at sound sources and recognizing sound classes
#It only uses the sound input, and agent output
import Inputs.SoundService as Sound_Service

from I2P.ttypes import *

#to ease client server creation in python
import ThriftTools

from numpy import array
import numpy as np
from random import choice

#time
import I2PTime
import transformations

#thread
import threading
import time

#the world model
import worldEntity

import math

#the class listening for sound inputs
class SoundServiceHandler:
    def sound(self, sensorID, timestamp, source):
        print 'Inside SoundServiceHandler'
        theWorld.soundDetected(source)
        
user_tracking_device = 'kinect1'
class WorldFusionHandler:
    def __init__(self):
        print "hello"
    def userEntered(self, sensorID, timestamp, userID, userName, detail):
        print sensorID
        if sensorID == user_tracking_device:
            print "inside userEntered"
            theWorld.prepareUserNew(userID,userName)
            print detail
        return
    def userLeft(self, sensorID, timestamp, userID, userName):
        if sensorID == user_tracking_device:
            theWorld.prepareUserLost(userID,userName)
        return
#the function updating position to look at
class WorldQueryHandler:
    def __init__(self):
        return
        
    def __del__(self):
        return
        
    def getLocation(self, target):
        targetLocation = EventPublisher.ttypes.Location()
        pos = I2P.ttypes.Vec3()
        pos.x = 0.0
        pos.y = 0.0
        pos.z = 0.0
        quat = I2P.ttypes.Vec4()
        quat.x = 0.0
        quat.y = 0.0
        quat.z = 0.0
        quat.w = 1.0
        #print 'look for target ' + target
        try:
            entity = theWorld.worldModel[target]
            #print 'target found'
            entityPosition = entity.getPosition()
            entityOrientation = entity.getOrientation()
            pos.x = entityPosition[0]
            pos.y = entityPosition[1]
            pos.z = entityPosition[2]
            quat.w = entityOrientation[0]
            quat.x = entityOrientation[1]
            quat.y = entityOrientation[2]
            quat.z = entityOrientation[3]
            
            #print pos
            targetLocation.isUnknown = False
        except KeyError:
            #print 'target NOT found'
            targetLocation.isUnknown = True
        targetLocation.location = pos 
        targetLocation.orientation = quat

        return targetLocation

class World(threading.Thread):
    def __init__(self,smartbody_client,multiUserTrackingAndFusion_client):
        threading.Thread.__init__(self)
        self.stopping = False
        self.smartbody_client = smartbody_client
        self.multiUserTrackingAndFusion_client = multiUserTrackingAndFusion_client
        
        self.lastSpeech = time.time()
        
        self.worldModel = dict()
        #initialize the scene
        display = worldEntity.WorldEntity('VH_Display_01')
        display.setPosition(array([0.6,0.65,1.8]))
        #display.setOrientation(array([1.0,0.0,0.0,0.0]))
        display.setOrientation(transformations.quaternion_about_axis(math.pi/4.0,array([0.0,0.0,1.0])))
        self.worldModel['VH_Display_01'] = display
        
        subject = worldEntity.WorldEntity('Subject')
        subject.setPosition(array([0.0,0.0,0.0]))
        subject.setOrientation(array([1.0,0.0,0.0,0.0]))
        self.worldModel['Subject'] = subject

        userTrack = worldEntity.WorldEntity(user_tracking_device)
        userTrack.setPosition(array([1.5,0.0,1.8]))
        userTrack.setOrientation(transformations.quaternion_about_axis(math.pi/2.0,array([0.0,0.0,1.0])))
        self.worldModel[user_tracking_device] = userTrack
        
        self.currentTarget = 'unknown'
        
        self.gesture = ''
        self.isRecoGesture = False 
        self.isNewUser = False
        self.isLostUser = False
        
    def run(self):
        while(not self.stopping):
            if self.isNewUser:
                self.doUserNew()
            if self.isLostUser:
                self.doUserLost()
            time.sleep(1)
        self.smartbody_client.disconnect()
        
    def soundDetected(self, source):
        print 'Inside SoundDetected'
        self.multiUserTrackingAndFusion_client.client.sendAudioLocalization(source)
        print source
        angle = source.azimuth
        soundtype = source.soundtype
        print 'input ',angle,soundtype
        pos = I2P.ttypes.Vec3()
        #in opengl cordinate
        scale = 100.0
        pos.x = scale*2.0*math.sin(angle)
        pos.y = scale*0.0
        pos.z = scale*2.0*math.cos(angle)
        print 'pos ', pos.x, pos.z
        if time.time() - self.lastSpeech > 2.0:
            if soundtype == SoundClass.SPEECH:
                #self.smartbody_client.client.setFaceExpression(I2P.ttypes.Facial_Expression.ANGER,10)
                sentence = choice(['Did you drop something?','I heard something drop','Something dropped there'])                
                #self.smartbody_client.client.speak(sentence,5) 
            elif soundtype == SoundClass.IMPULSIVE_SOUND:
                #self.smartbody_client.client.setFaceExpression(I2P.ttypes.Facial_Expression.SURPRISE,10)
                sentence = choice(['Did you drop something?','I heard something drop','Something dropped there'])
                self.smartbody_client.client.speak(sentence,5)
                #self.smartbody_client.client.lookAtPosition(pos)
            else:
                sentence = choice(['Someones phone is ringing','I hear phone ringing','pick up your phone'])
                self.smartbody_client.client.speak(sentence,5)
            self.lastSpeech = time.time()
            self.smartbody_client.client.lookAtPosition(pos)
        
    def stop(self):
        self.stopping = True
        
    def prepareUserNew(self,userID, userName):
        self.isNewUser = True
        self.newUserName = userName
        print self.newUserName
        print "inside prepareUserNew"
        
    def prepareUserLost(self,userID,userName):
        self.isLostUser = True
        self.lostUserName = userName
        print self.lostUserName

    def doUserNew(self):
        #self.smartbody_client.client.lookAtTarget('Subject')
        self.smartbody_client.client.setFaceExpression(I2P.ttypes.Facial_Expression.HAPPINESS,10)
        self.smartbody_client.client.playAnimation(Control.ttypes.Animation.WAVE_HAND)
        self.smartbody_client.client.speak('Hi, I am Chloe.',10)
        print "inside doUserNew"
        time.sleep(5.0)
        self.isNewUser = False
    
    def doUserLost(self):
        self.smartbody_client.client.playAnimation(Control.ttypes.Animation.WAVE_HAND)
        self.smartbody_client.client.setFaceExpression(I2P.ttypes.Facial_Expression.SADNESS,10)
        self.smartbody_client.client.speak('Good bye. I hope to see you again.',10)
        print self.lostUserName
        self.smartbody_client.client.endLookAt()
        self.isLostUser = False
            
def convertFromVec3ToArray( vec ):
    return array([vec.x, vec.y, vec.z])
    
def convertFromOpenGLToWorld( vec ):
    return array([vec[2],vec[0],vec[1]])
        
def readKeyboard1():
    found = False
    # exit condition
    res = -2
    while not found:
        res = raw_input('Enter s to start virtual human client. \
                         Press enter after the key entry.\n')
        if len(res) != 1:
            print ('Wrong option\n')
        else:
            # test exit
            if res[0] == 's':
                res = -1
                found = True
                continue
    return res
    
    
def readKeyboard2():
    found = False
    # exit condition
    res = -2
    while not found:
        res = raw_input('Enter q to exit. \
                         Press enter after the key entry.\n')
        if len(res) != 1:
            print ('Wrong option\n')
        else:
            # test exit
            if res[0] == 'q':
                res = -1
                found = True
                continue
    return res
    
if __name__ == "__main__":

  #starting user tracking and gesture recognition service
    worldFusion_handler = WorldFusionHandler()
    worldFusion_server = ThriftTools.ThriftServerThread(Inputs.constants.DEFAULT_WORLDFUSION_SERVICE_PORT,\
        WorldFusion_Service,worldFusion_handler,'World Fusion server','localhost')
		#UserTracking_Service,userTracking_handler,'User Tracking server','localhost')
    worldFusion_server.start()
    
    #starting worldQuery
    worldQuery_handler = WorldQueryHandler()
    worldQuery_server = ThriftTools.ThriftServerThread(EventPublisher.constants.DEFAULT_WORLD_QUERY_PORT,WorldQuery_Service,worldQuery_handler,'World Query')
    worldQuery_server.start()
    
    #starting gesture recognition service
    sound_handler = SoundServiceHandler()
    sound_server = ThriftTools.ThriftServerThread(Inputs.constants.DEFAULT_SOUND_SERVICE_PORT,Sound_Service,\
    sound_handler,'Sound server','155.69.52.73')
    sound_server.start()
    
    
    
    res = readKeyboard1()
    while res != -1:
        res = readKeyboard1()
        
    smartbody_client = ThriftTools.ThriftClient('localhost',9090,AgentControl_Service,'SmartBody')
    smartbody_client.connect()
    
    #create client here to connect to multiUserTrackingAndFusion
    multiUserTrackingAndFusion_client = ThriftTools.ThriftClient('localhost',9091,FusionControl_Service,'multiUserTrackingAndFusion')
    multiUserTrackingAndFusion_client.connect()
    
    theWorld = World(smartbody_client,multiUserTrackingAndFusion_client)
    theWorld.start()
    
    servers = [sound_server,worldFusion_server,worldQuery_server,theWorld]
    
    res = readKeyboard2()
    while res != -1:
        res = readKeyboard2()
    for s in servers:
        s.stop()
    print 'done'