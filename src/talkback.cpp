#include "StdAfx.h"
#include <windows.h>
#include <sapi.h>
#include <stdio.h>
#include <string.h>
#include <atlbase.h>
#include "sphelper.h"
// we can compile language only if we use windows cmd, it won't work with git command shell 
#include "grammar.h"
//Copyright (c) Microsoft Corporation. All rights reserved.

// I2p headers
#include "AgentControl.h"
#include "I2P_types.h"
#include "ThriftTools.hpp"
//General headers
#include "SpeechClient.h"

inline HRESULT BlockForResult(ISpRecoContext * pRecoCtxt, ISpRecoResult ** ppResult)
{
    HRESULT hr = S_OK;
	CSpEvent event;

    printf("waiting to recognize...\n");
    while (SUCCEEDED(hr) &&
           SUCCEEDED(hr = event.GetFrom(pRecoCtxt)) &&
           hr == S_FALSE)
    {        
        hr = pRecoCtxt->WaitForNotifyEvent(INFINITE);
    }
    switch (event.eEventId)
        {
            case SPEI_RECOGNITION:
            {
                // ExecuteCommand(event.RecoResult(), hWnd);
                *ppResult = event.RecoResult();
                if (*ppResult)
                {
                    (*ppResult)->AddRef();
                }                
                printf("Done proper recogntion\n");
            }
        }
    // printf("done with recognition\n");
    return hr;
}

const WCHAR * StopWord()
{
    const WCHAR * pchStop;
    
    LANGID LangId = ::SpGetUserDefaultUILanguage();

    switch (LangId)
    {
        case MAKELANGID(LANG_JAPANESE, SUBLANG_DEFAULT):
            pchStop = L"\x7d42\x4e86\\\x30b7\x30e5\x30fc\x30ea\x30e7\x30fc/\x3057\x3085\x3046\x308a\x3087\x3046";;
            break;

        default:
            pchStop = L"bye";
            break;
    }

    return pchStop;
}
            
int main(int argc, char* argv[])
{
    HRESULT hr = E_FAIL;
    bool fUseTTS = true;            // turn TTS play back on or off
    bool fReplay = true;            // turn Audio replay on or off
    SpeechClient<imi::AgentControlClient> *s_client = new SpeechClient<imi::AgentControlClient>("localhost",9090);
    
    // Process optional arguments
    if (argc > 1)
    {
        int i;

        for (i = 1; i < argc; i++)
        {
            if (_stricmp(argv[i], "-noTTS") == 0)
            {
                fUseTTS = false;
                continue;
            }
            if (_stricmp(argv[i], "-noReplay") == 0)
            {
                fReplay = false;
                continue;
            }       
            printf ("Usage: %s [-noTTS] [-noReplay] \n", argv[0]);
            return hr;
        }
    }

    if (SUCCEEDED(hr = ::CoInitialize(NULL)))
	{
        {            
            CComPtr<ISpRecognizer> cpRecognizer;
            CComPtr<ISpRecoContext> cpRecoCtxt;
            CComPtr<ISpRecoGrammar> cpGrammar;
            
            // This is half baked code for InProc recognizer
            hr = cpRecognizer.CoCreateInstance(CLSID_SpInprocRecognizer);
            //if (SUCCEEDED(hr))
            //{
            //    // this is somewhat I am not able to make lead, I have to mention 
            //    // the type to SAPI Sample something as show in RECO example.
            //    hr = cpRecognizer->SetRecognizer((ISpObjectToken *)ItemData);
            //}
            if (SUCCEEDED(hr))
            {
                CComPtr<ISpObjectToken> cpAudioToken;
                hr = SpGetDefaultTokenFromCategoryId(SPCAT_AUDIOIN, &cpAudioToken);
                if (SUCCEEDED(hr))
                {
                    hr = cpRecognizer->SetInput(cpAudioToken, TRUE);
                }
            }

            /*hr = cpRecognizer.CoCreateInstance(CLSID_SpSharedRecognizer);*/
            if (SUCCEEDED(hr))
            {
                hr = cpRecognizer->CreateRecoContext(&cpRecoCtxt);
            }            
            hr = cpRecoCtxt->SetNotifyWin32Event();
            if(FAILED(hr))
            {
                printf("Error setting notification event\n") ;
                return hr;
            }
            const ULONGLONG ullDictInterest(SPFEI(SPEI_RECOGNITION));
            hr = cpRecoCtxt->SetInterest(ullDictInterest, ullDictInterest);
            // hr = cpRecoCtxt->GetVoice( &cpVoice );
            if(FAILED(hr))
            {
                printf("Error setting interest\n") ;
                return hr;
            }            
            
            // 409 taken from xml file, which is taken from coffee example :P
            // here is one more reference 
            // http://msdn.microsoft.com/en-us/library/ms723630(v=vs.85).aspx
            hr = cpRecoCtxt->CreateGrammar(409, &cpGrammar);
            if (SUCCEEDED(hr))
                hr = cpGrammar->LoadCmdFromFile(L"../src/grammar.xml", SPLO_DYNAMIC);            
            hr = cpGrammar->SetRuleState(NULL, NULL, SPRS_ACTIVE );
            // hr = cpGrammar->SetRuleState(VID_Navigation, SPRS_ACTIVE );
            if (SUCCEEDED(hr))
            {   
                USES_CONVERSION;
                const WCHAR * const pchStop = StopWord();
                CComPtr<ISpRecoResult> cpResult;
                printf( "I will repeat everything you say.\nSay \"%s\" to exit.\n", W2A(pchStop) );
                while (SUCCEEDED(hr = BlockForResult(cpRecoCtxt, &cpResult)))
                {    
                    cpGrammar->SetDictationState( SPRS_INACTIVE );
                    CSpDynamicString dstrText;                    
                    if (SUCCEEDED(cpResult->GetText(SP_GETWHOLEPHRASE, SP_GETWHOLEPHRASE, 
                                                    TRUE, &dstrText, NULL)))
                    {
                        SPPHRASE *pElements;
                        if (SUCCEEDED(cpResult->GetPhrase(&pElements)))
                        {
                            switch ( pElements->Rule.ulId )
                            {
                                case VID_Navigation:
                                    printf("I heard:  %s\n", W2A(dstrText));
									if(_wcsicmp(dstrText, L"Hi") == 0)
										s_client->getClient()->speak( "Hello", 60.0);
									if(_wcsicmp(dstrText, L"How are you?") == 0)
										s_client->getClient()->speak( "I am fine. How are you?", 60.0);
									if(_wcsicmp(dstrText, L"What's your name?") == 0)
										s_client->getClient()->speak( "My Name is Sophie. Nice to meet you", 60.0);	
                                    break;
                                default:
                                    printf("Something I was not expecting, %s\n", W2A(dstrText));
                            }
                        }
                        cpResult.Release();
                    }
                    if (_wcsicmp(dstrText, pchStop) == 0)
                    {
                        printf("Closing gracefully\n");
                        cpGrammar.Release();
                        cpRecoCtxt.Release();
                        cpRecognizer.Release();
                        break;
                    }                                        
                    cpGrammar->SetDictationState( SPRS_ACTIVE );
                } 
            }
            else 
                printf("Something got screwed up!\n");
        }
        ::CoUninitialize();
    }
    return hr;
}
