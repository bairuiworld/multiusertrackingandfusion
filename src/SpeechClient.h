#ifndef _SpeechClient_H
#define _SpeechClient_H

#include <thrift/transport/TSocket.h>
#include <thrift/transport/TBufferTransports.h>
#include <thrift/protocol/TBinaryProtocol.h>

#include <iostream>

using namespace apache::thrift;
using namespace apache::thrift::protocol;
using namespace apache::thrift::transport;
template<class ClientT>

//#include "WorldQuery.h"

class SpeechClient
{
public:
    SpeechClient(std::string host, int port);
    virtual ~SpeechClient();

    void connect();
    bool isConnected();
    bool ensureConnection();
	void receiveNetworkException();
    boost::shared_ptr<ClientT> getClient();

private:
    
    std::string m_address;
    int m_port;
	bool m_hasNetwokException;
        
    /// The socket held for connecting to the client.
    boost::shared_ptr< ::apache::thrift::transport::TSocket> m_socket;   
    boost::shared_ptr<ClientT> m_client;
};

template <class ClientT>
SpeechClient<ClientT>
::SpeechClient(std::string host, int port):
    m_address(host),
    m_port(port),
	m_hasNetwokException(false)
{
    ensureConnection();
}

template <class ClientT>
SpeechClient<ClientT>
::~SpeechClient()
{
    try
    {
        m_socket->close();
    } 
    catch( apache::thrift::TException &tx ) 
    {
        std::cerr << "EXCEPTION closing the network conn: " << tx.what() << "\n";
    }    
}

template <class ClientT>
bool 
SpeechClient<ClientT>
::isConnected( void ) 
{ 
    return m_socket && m_socket->isOpen() && !m_hasNetwokException; 
}


// Attempt to connect back to subscriber
template <class ClientT>
void 
SpeechClient<ClientT>
::connect( void )
{
    std::cerr << "Attempting to connect to " << m_address << " " << m_port;
    try 
    {
        m_socket.reset(new ::apache::thrift::transport::TSocket(m_address.c_str(), m_port));
        boost::shared_ptr<TTransport> transport(new TBufferedTransport(m_socket));
        boost::shared_ptr<TProtocol> protocol(new TBinaryProtocol(transport));
        
        m_client.reset( new ClientT(protocol) );
        
        // Attempt to connect
        transport->open();
		m_hasNetwokException = false;
    }     
    catch( apache::thrift::TException &tx ) 
    {
        std::cerr << "Protected Client - EXCEPTION opening the network conn: " << tx.what() << "\n";
    }
}

template <class ClientT>
bool 
SpeechClient<ClientT>
::ensureConnection(void)
{
    bool isRetry = false;
    while( !isConnected())
    {
        connect();
        if(isRetry) 
        {
            sleep( 1 );
        }
        isRetry = true;
    }
    return true;
}

template <class ClientT>
boost::shared_ptr<ClientT>
SpeechClient<ClientT>
::getClient()
{
    return m_client;
}

template <class ClientT>
void
SpeechClient<ClientT>
::receiveNetworkException()
{
    m_hasNetwokException = true;
}


#endif