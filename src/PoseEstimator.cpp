#include <iostream>
#include <string>
#include "Global.h"
#include "ThriftTools.hpp"
#include "PoseEstimator.h"


using namespace std;

PoseEstimator::PoseEstimator()
{
	// w_client = new ProtectedClient<imi::UserTrackingServiceClient>("localhost",imi::g_Inputs_constants.DEFAULT_USERTRACKING_SERVICE_PORT);
	w_client = new ProtectedClient<imi::WorldFusionServiceClient>("localhost",imi::g_Inputs_constants.DEFAULT_WORLDFUSION_SERVICE_PORT);

	m_serverToClient= new ServerToClient();
	mFusionControlServerArguments = new FusionControlServerArguments(m_serverToClient);
	boost::thread server(startFusionControlServer,mFusionControlServerArguments);

}

PoseEstimator::~PoseEstimator()
{
	delete w_client;	w_client = NULL;
}

// Kinect action template (mean must be subtracted!!!)
double PoseEstimator::m_TemplateKinect[11][12] = 
{
	{-0.073,0.117,-0.098,0.081,-0.146,-0.073,0.175,0.036,-0.186,0.195,0.304,-0.333},	// object
	{-0.071,0.119,-0.098,0.082,-0.146,-0.075,0.148,0.053,-0.195,0.181,0.304,-0.302},	// question
	{0.002,0.183,-0.005,0.134,-0.103,0.017,0.234,-0.163,-0.157,0.359,-0.032,-0.469},	// praise
	{-0.010,0.172,-0.038,0.139,-0.176,-0.032,0.286,-0.296,-0.153,0.521,-0.208,-0.205},	// weakly agree
	{-0.029,0.137,-0.046,0.117,-0.157,-0.013,0.283,-0.208,-0.145,0.190,0.064,-0.195},	// confident
	{-0.018,0.154,-0.058,0.116,-0.169,-0.025,0.244,-0.147,-0.156,0.111,0.066,-0.118},	// calling
	{0.003,0.201,-0.017,0.153,-0.122,-0.029,0.277,-0.091,-0.292,0.348,0.068,-0.501},	// stop
	{0.011,0.205,-0.019,0.159,-0.117,-0.025,0.289,-0.102,-0.296,0.375,0.026,-0.506},	// successful
	{0.059,0.260,0.006,0.234,-0.077,0.061,0.214,-0.178,-0.137,0.043,-0.056,-0.427},		// reading
	{0.048,0.231,-0.019,0.203,-0.109,0.052,0.245,-0.289,-0.033,0.297,-0.281,-0.346},	// writing
	{0.015,0.237,0.006,0.166,-0.157,-0.021,0.269,-0.212,-0.197,0.123,0.049,-0.278}		// drinking

};

// Kinect root of the sum of power value of action templates
double PoseEstimator::m_TemplatePowSumRootKinect[11] =
{0.608,0.585,0.720,0.795,0.532,0.454,0.792,0.813,0.647,0.738,0.600};

// Glove action template (mean must be subtracted!!!)
double PoseEstimator::m_TemplateGlove[11][20] =
{
	{0.359,-0.494,0.316,-1.117,0.024,0.965,-0.715,-0.492,0.380,1.113,-1.559,-0.712,0.399,1.813,-0.686,-0.700,0.443,1.984,-0.615,-0.706},		// object
	{0.325,-0.350,0.230,-0.746,-0.527,0.306,-0.003,0.086,-0.499,0.232,-0.007,0.059,-0.493,0.534,0.098,0.145,-0.162,0.379,0.087,0.306},			// question
	{-0.399,-1.006,-0.310,-1.343,0.234,1.154,-0.479,-0.313,0.560,1.349,-1.413,-0.560,0.545,1.725,-0.541,-0.603,0.372,2.202,-0.510,-0.663},		// praise
	{-0.254,-0.186,0.205,-0.721,-0.374,0.150,-0.020,0.116,-0.438,0.225,-0.106,0.008,-0.271,0.527,0.052,0.119,-0.071,0.845,0.062,0.130},			// weakly agree
	{0.256,-0.342,0.535,-1.125,0.064,1.073,-1.243,-0.493,0.406,1.195,-1.557,-0.721,0.426,1.858,-0.695,-0.715,0.401,2.067,-0.663,-0.727},		// confident
	{-0.271,-0.656,-0.131,-0.893,-0.321,-0.072,-0.197,-0.167,-0.436,0.634,-0.265,-0.216,-0.283,1.324,-0.160,-0.039,0.023,2.006,-0.170,0.289},		// calling
	{-0.143,-0.101,0.405,-0.718,-0.667,0.249,0.145,0.226,-1.002,0.187,0.195,0.190,-0.673,0.527,0.195,0.215,-0.310,0.642,0.205,0.233},			// stop
	{0.398,-0.155,0.141,-0.667,-0.734,-0.462,-0.330,-0.627,-0.865,-0.348,-0.246,-0.420,0.126,1.779,-0.341,0.149,0.285,2.309,-0.330,0.340},		// successful
	{0.587,-0.416,-0.300,-0.716,0.234,-0.183,-0.375,-0.189,0.442,-0.011,-0.510,-0.350,0.561,0.523,-0.300,-0.308,0.964,0.865,-0.300,-0.218},		// reading
	{0.313,-0.556,0.190,-0.784,0.230,-0.002,-0.525,-0.210,0.476,0.243,-0.531,-0.385,0.403,0.882,-0.363,-0.428,0.263,1.650,-0.362,-0.505},		// writing
	{0.633,-0.777,0.405,-0.624,-0.239,0.361,-0.366,-0.220,0.036,0.192,-0.517,-0.256,0.234,0.636,-0.244,-0.158,0.559,0.505,-0.242,0.081}			// drinking
};

// Glove root of the sum of power value of action templates
double PoseEstimator::m_TemplatePowSumRootGlove[11] =
{4.140,1.534,4.341,1.483,4.387,2.863,1.956,3.459,2.134,2.565,1.864};

// Action title
std::string PoseEstimator::m_ActionTitle[11] =
{
	"Object!", "Have Question!", "Praise!", "Weakly Agree!", "Confident!", "Calling!", "Stop!", "Successful!", "Reading!", "Writing!", "Drinking!"
};

bool PoseEstimator::Init(void)
{
	// action type parameters
	m_numAction = 11;
	m_lenFeatureGlove = 20;
	m_corrThd = 0.87;
	m_corrThd1 = 0.93;	// for "writing!"

	// temporal smoothing parameters for final action judgment
	m_actionIdx = -1;
	m_numJudgeFrame = 6;
	m_JudgeActionIdx.reserve(m_numJudgeFrame);
	for (int i = 0; i < m_numJudgeFrame; i++)
	{
		m_JudgeActionIdx.push_back(-1);
	}

	// open the kinect camera
	//m_gReader.Init(w_client,s_client);
	m_gReader.Init(w_client);
	m_gReader.SetFrameScale(1.0);

	if (!m_gReader.IsOpen())
	{
		if(!m_gReader.OpenCam())
			return false;
	}

	// connect to the glove.
	/*try
	{
		m_pGloveDict = vhtIOConn::getDefault( vhtIOConn::glove);
		m_pGlove = new vhtCyberGlove(m_pGloveDict);
	}
	catch (vhtBaseException *e)
	{
		printf("Error: %s\nPress <enter> to exit.\n", e->getMessage());
		getchar();
		return false;
	}*/
	return true;

}

void PoseEstimator::Clear(void)
{
	// close the kinect camera
	if (m_gReader.IsOpen())
		m_gReader.CloseCam();
	m_gReader.Clear();
}

void PoseEstimator::ViewVideo(void)
{
	while (1)
	{
		Update();
		if( cv::waitKey(50) >= 0 )
		{
			break;
		}
	}
}

void PoseEstimator::Update(void)
{
	//m_pGlove->update();		// update Glove data

	m_gReader.GetOneFrame();
	m_gReader.GetActionFeature();	// extract Kinect action feature
	m_gReader.ShowFrame();

	// get the finger pose
	//double fPoseParams[20];
//	int nPoseIndex = 0;
////	FILE *pFile = fopen("F:\\hand_motion.txt", "a");
//	for( int nFingerNum = 0; nFingerNum < GHM::nbrFingers; nFingerNum++ )
//	{
//		string strJointName;
//		DofType dtType;
//		cout << nFingerNum << ": Value: ";
//		for( int nJointNum = 0; nJointNum < GHM::nbrJoints; nJointNum++ )
//		{
//			double fValue = m_pGlove->getData( (GHM::Fingers)nFingerNum, (GHM::Joints)nJointNum);
//			if (nJointNum == 0 && nFingerNum == 0)
//				cout <<  fValue << " ";
//			GetFlexionIndex(nFingerNum, nJointNum, strJointName, dtType);
//			double fDofValue = -1.0 * fValue;
////			fprintf(pFile, "%lf ", fDofValue);
//			m_gloveFea[nPoseIndex++] = fDofValue;
//		}
//		double fValue = m_pGlove->getData((GHM::Fingers)nFingerNum, GHM::abduct );
//		double fSign[5] = {1.0, 1.0, 1.0, 1.0, 1.0};
//		if (nFingerNum == 0)
//			cout << "Adduct: " << fValue << std::endl;
//
//		GetAdductIndex(nFingerNum, strJointName, dtType);
//		double fDofValue = fSign[nFingerNum] * fValue;
////		fprintf(pFile, "%lf ", fDofValue);
//		m_gloveFea[nPoseIndex++] = fDofValue;
//	}
//	fprintf(pFile, "\n");
//	fclose(pFile);

//	SaveFrame();	// save samples for training

	//RecognzieGesture();		// recognize action or gesture

}

void PoseEstimator::GetAdductIndex(int nFingerNum, std::string &strJointName, DofType &dtType)
{
	if (nFingerNum == 0)	{ strJointName = "ThumbTM";		dtType = ROTATION_Y; }
	if (nFingerNum == 1)	{ strJointName = "IndexMCP";		dtType = ROTATION_Z; }
	if (nFingerNum == 2)	{ strJointName = "MiddleMCP";	dtType = ROTATION_Z; }
	if (nFingerNum == 3)	{ strJointName = "RingMCP";		dtType = ROTATION_Z; }
	if (nFingerNum == 4)	{ strJointName = "PinkyMCP";		dtType = ROTATION_Z; }
}

void PoseEstimator::GetFlexionIndex(int nFingerNum, int nJointNum,
	std::string &strJointName, DofType &dtType)
{
	if (nFingerNum == 0 && nJointNum == 0)	{ strJointName = "ThumbTM";		dtType = ROTATION_Z; }
	if (nFingerNum == 0 && nJointNum == 1)	{ strJointName = "ThumbMCP";	dtType = ROTATION_Z; }
	if (nFingerNum == 0 && nJointNum == 2)	{ strJointName = "ThumbIP";		dtType = ROTATION_Z; }
	if (nFingerNum == 1 && nJointNum == 0)	{ strJointName = "IndexMCP";		dtType = ROTATION_Y; }
	if (nFingerNum == 1 && nJointNum == 1)	{ strJointName = "IndexPIP";		dtType = ROTATION_Y; }
	if (nFingerNum == 1 && nJointNum == 2)	{ strJointName = "IndexDIP";		dtType = ROTATION_Y; }
	if (nFingerNum == 2 && nJointNum == 0)	{ strJointName = "MiddleMCP";	dtType = ROTATION_Y; }
	if (nFingerNum == 2 && nJointNum == 1)	{ strJointName = "MiddlePIP";		dtType = ROTATION_Y; }
	if (nFingerNum == 2 && nJointNum == 2)	{ strJointName = "MiddleDIP";		dtType = ROTATION_Y; }
	if (nFingerNum == 3 && nJointNum == 0)	{ strJointName = "RingMCP";		dtType = ROTATION_Y; }
	if (nFingerNum == 3 && nJointNum == 1)	{ strJointName = "RingPIP";		dtType = ROTATION_Y; }
	if (nFingerNum == 3 && nJointNum == 2)	{ strJointName = "RingDIP";		dtType = ROTATION_Y; }
	if (nFingerNum == 4 && nJointNum == 0)	{ strJointName = "PinkyMCP";		dtType = ROTATION_Y; }
	if (nFingerNum == 4 && nJointNum == 1)	{ strJointName = "PinkyPIP";		dtType = ROTATION_Y; }
	if (nFingerNum == 4 && nJointNum == 2)	{ strJointName = "PinkyDIP";		dtType = ROTATION_Y; }
}

void PoseEstimator::RecognzieGesture(void)
{
	int i = 0;

	GetActionCorr(KINECT);	// kinect correlation coefficients 
	GetActionCorr(GLOVE);	// glove correlation coefficients

	double maxCorr = m_corrKinect[0] * m_corrGlove[0];
	int actionIdx = 0;

	for (i = 1; i < m_numAction; i++)
	{
		if (maxCorr < m_corrKinect[i] * m_corrGlove[i])
		{
			maxCorr = m_corrKinect[i] * m_corrGlove[i];
			actionIdx = i;
		}
	}

	if (maxCorr < pow(m_corrThd,2))
	{
		actionIdx = -1;	// no meaningful action or gesture
	}

	if (actionIdx == 9) // fro "writing!"
	{
		if (maxCorr < pow(m_corrThd1,2))
		{
			actionIdx = -1;	// no meaningful action or gesture
		}
	}

	// temporal smoothing for final action 
	TemporalSmoothing(actionIdx);

	ShowActionType(m_actionIdx);

}

double PoseEstimator::FeaMean(double *feaVector, int feaLength)
{
	int i = 0;

	double feaMean = 0;

	for (i = 0; i < feaLength; i++)
	{
		feaMean = feaMean + feaVector[i];
	}

	return feaMean / feaLength;
}

void PoseEstimator::GetActionCorr(DeviceIndex deviceIdx)
{
	int i = 0, j= 0;

	double sumTmp = 0, sumTmp1 = 0;
	double diffTmp = 0, feaMean =0;

	if (deviceIdx == KINECT)
	{
		feaMean = FeaMean(m_gReader.m_feaKinect, m_gReader.m_lenFeaKinect);	// mean of feature vector

		for (i = 0; i < m_numAction; i++)
		{
			sumTmp = 0, sumTmp1 = 0;

			for (j = 0; j < m_gReader.m_lenFeaKinect; j++)
			{
				diffTmp = m_gReader.m_feaKinect[j] - feaMean;
				sumTmp = sumTmp + diffTmp * m_TemplateKinect[i][j];
				sumTmp1 = sumTmp1 + pow(diffTmp, 2.0);
			}

			if (sumTmp1 == 0)
			{
				m_corrKinect[i] = 0;
			} 
			else
			{
				m_corrKinect[i] = sumTmp / (sqrt(sumTmp1) * m_TemplatePowSumRootKinect[i]);
			}

		}
	} 
	else
	{
		feaMean = FeaMean(m_gloveFea, m_lenFeatureGlove);	// mean of feature vector

		for (i = 0; i < m_numAction; i++)
		{
			sumTmp = 0, sumTmp1 = 0;

			for (j = 0; j < m_lenFeatureGlove; j++)
			{
				diffTmp = m_gloveFea[j] - feaMean;
				sumTmp = sumTmp + diffTmp * m_TemplateGlove[i][j];
				sumTmp1 = sumTmp1 + pow(diffTmp, 2.0);
			}

			if (sumTmp1 == 0)
			{
				m_corrGlove[i] = 0;
			} 
			else
			{
				m_corrGlove[i] = sumTmp / (sqrt(sumTmp1) * m_TemplatePowSumRootGlove[i]);
			}
		}
	}
}

void PoseEstimator::SaveFrame(void)
{
	// save Kinet and Glove feature data to binary files
	int i = 0;
	
	char text[255];
	sprintf(text, "D:\\Pose_Database\\depth_%d.dat", m_gReader.m_nFrmNum);
	FILE *pFile = fopen(text, "wb");

	// Kinect feature data
	for (i = 0; i < m_gReader.m_lenFeaKinect; i++)
	{
		if (i != m_gReader.m_lenFeaKinect-1)
		{
			fprintf(pFile, "%f\t", m_gReader.m_feaKinect[i]);
		} 
		else
		{
			fprintf(pFile, "%f\n", m_gReader.m_feaKinect[i]);
		}
	}

	// Glove feature data
	for (i = 0; i < m_lenFeatureGlove; i++)
	{
		fprintf(pFile, "%f\t", m_gloveFea[i]);
	}

	fclose(pFile);

	// save Kinect RGB imgae
	sprintf(text, "D:\\Pose_Database\\color_%d.jpg", m_gReader.m_nFrmNum);
	imwrite(text, m_gReader.m_mtxColorImg);
}

void PoseEstimator::ShowActionType(int actionIdx)
{
	// background image
	cv::Mat actionIdxImg(300, 500, CV_8UC3, cv::Scalar(255,255,255));
	IplImage actionIdxImg1 = IplImage(actionIdxImg);

	// text font
	CvFont font; 
	cvInitFont(&font, CV_FONT_HERSHEY_DUPLEX,2, 2, 0, 1, CV_AA);

	// text size
	char text[30];
	CvSize textSize;
	int baseline;

	if (actionIdx == -1)	// no meaningful action
	{
		sprintf(text, " ");
	}
	else
	{
		sprintf(text, m_ActionTitle[actionIdx].c_str());
		try 
		{
			switch(actionIdx)
			{
				case 0:
					//w_client->getClient()->gestureStart(std::string("Cyberglove"),imi::getTimeStamp(),0,imi::Gesture::OBJECTION);
					break;
				case 1:
					//w_client->getClient()->gestureStart(std::string("Cyberglove"),imi::getTimeStamp(),0,imi::Gesture::HAVINGQUESTION);
					break;
				case 2:
					//w_client->getClient()->gestureStart(std::string("Cyberglove"),imi::getTimeStamp(),0,imi::Gesture::PRAISE);
					break;
				case 3:
					//w_client->getClient()->gestureStart(std::string("Cyberglove"),imi::getTimeStamp(),0,imi::Gesture::AGREEMENT);
					break;
				case 4:
					//w_client->getClient()->gestureStart(std::string("Cyberglove"),imi::getTimeStamp(),0,imi::Gesture::CONFIDENT);
					break;
				case 5:
					//w_client->getClient()->gestureStart(std::string("Cyberglove"),imi::getTimeStamp(),0,imi::Gesture::ONCALL);
					break;
				case 6:
					//w_client->getClient()->gestureStart(std::string("Cyberglove"),imi::getTimeStamp(),0,imi::Gesture::STOP);
					break;
				case 7:
					//w_client->getClient()->gestureStart(std::string("Cyberglove"),imi::getTimeStamp(),0,imi::Gesture::SUCCESS);
					break;
				case 8:
					//w_client->getClient()->gestureStart(std::string("Cyberglove"),imi::getTimeStamp(),0,imi::Gesture::READ);
					break;
				case 9:
					//w_client->getClient()->gestureStart(std::string("Cyberglove"),imi::getTimeStamp(),0,imi::Gesture::WRITE);
					break;
				case 10:
					//w_client->getClient()->gestureStart(std::string("Cyberglove"),imi::getTimeStamp(),0,imi::Gesture::DRINK);
					break;
			}			
		}   
		catch( apache::thrift::TException &tx ) 
		{
			std::cerr << "EXCEPTION opening the network conn: " << tx.what() << "\n";
			w_client->receiveNetworkException();
		}
	}

	cvGetTextSize(text, &font, &textSize, &baseline);
	cvPutText(&actionIdxImg1, text, cvPoint((int)(actionIdxImg.cols-textSize.width)/2, (int)(actionIdxImg.rows-textSize.height)/2), &font, CV_RGB(255,0,0));

	imshow("action", actionIdxImg);


}

void PoseEstimator::TemporalSmoothing(int currActionIdx)
{
	for (vector<int>::iterator ite = m_JudgeActionIdx.begin(); ite!=m_JudgeActionIdx.end(); ite++)
	{
		if (currActionIdx != *ite)
		{
			m_actionIdx = -1;
			m_JudgeActionIdx.erase(m_JudgeActionIdx.begin());
			m_JudgeActionIdx.push_back(currActionIdx);
			return;
		}
	}

	m_actionIdx = currActionIdx;
}

// void PoseEstimator::GetActionCorr(double *feaVector, double **actionTemplate, double *actionTemplatePowSumRoot, int actionNum, int feaLength, DeviceIndex deviceIdx)
// {
// 	int i = 0, j= 0;
// 
// 	double sumTmp = 0, sumTmp1 = 0;
// 
// 	double feaMean = FeaMean(feaVector, feaLength);	// mean of feature vector
// 
// 	for (i=0; i < actionNum; i++)
// 	{
// 		for (j=0; j < feaLength; j++)
// 		{
// 			double diffTmp = feaVector[j] - feaMean;
// 			sumTmp = sumTmp + diffTmp * actionTemplate[i][j];
// 			sumTmp1 = sumTmp1 + pow(diffTmp, 2.0);
// 		}
// 
// 		if (deviceIdx == KINECT)	// Kinect correlation coefficients
// 		{
// 			if (sumTmp1 == 0)
// 			{
// 				m_corrKinect[i] = 0;
// 			} 
// 			else
// 			{
// 				m_corrKinect[i] = sumTmp / (sqrt(sumTmp1) * actionTemplatePowSumRoot[i]);
// 			}
// 		} 
// 		else	// Glove correlation coefficients
// 		{
// 			if (sumTmp1 == 0)
// 			{
// 				m_corrGlove[i] = 0;
// 			} 
// 			else
// 			{
// 				m_corrGlove[i] = sumTmp / (sqrt(sumTmp1) * actionTemplatePowSumRoot[i]);
// 			}
// 		}
// 
// 	}
// 
// }