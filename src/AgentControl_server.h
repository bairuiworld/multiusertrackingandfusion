#ifndef AgentControl_server_H
#define AgentControl_server_H

#include <string>
#include "AgentControl.h"
#include "ServerToClient.h"
#include "I2P_types.h"

struct AgentControlServerArguments
{/*
	std::vector<imi::AgentControl_moveToPosition_args>& _worldPosition;*/
	ServerToClient* stc;
    AgentControlServerArguments(ServerToClient* ostc)
	:stc(ostc)
    {
    }

};

void startAgentControlServer(AgentControlServerArguments*);

#endif