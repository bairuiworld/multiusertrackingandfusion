#pragma once
#if defined( _WIN32 )
#define _WIN32_WINNT 0x0400		
#endif
//include this first
#include "ThriftTools.hpp"
#include "WorldFusionService.h"
#include "ProtectedClient.h"
#include "Inputs_constants.h"
#include "EventPublisher_constants.h"
#include "ServerToClient.h"
#include "FusionControl_server.h"
//include this after to prevent win sock conflicts
#include <vector>
#include <algorithm>
#include <vhtBase.h>
#include "KinectReader.h"

// the ENUM Types related to the hand elements
enum FingerIndex {THUMB, INDEX, MIDDLE, RING, PINKY, NOT_A_FINGER};
enum HandPartLabel{PALM, THUMB0, THUMB1, THUMB2, INDEX0, INDEX1, INDEX2, MIDDLE0, 
	MIDDLE1, MIDDLE2, RING0, RING1, RING2, PINKY0, PINKY1, PINKY2, NOT_A_HAND_PART};
enum TargetPartLabel{PALM_T, THUMB_T, INDEX_T, MIDDLE_T, RING_T, PINKY_T, NOT_A_T_LABEL};
enum DofType {ROTATION_X, ROTATION_Y, ROTATION_Z, TRANSLATION_X, TRANSLATION_Y, TRANSLATION_Z, NOT_A_DOF};

// the the ENUM Types related to devices
enum DeviceIndex {KINECT, GLOVE};

class PoseEstimator
{
public:
	PoseEstimator();
	~PoseEstimator();

public:
	bool	Init(void);
	void	Clear(void);
	void	Update(void);
	void	ViewVideo(void);

private:
	void	GetAdductIndex(int nFingerNum, std::string &strJointName, DofType &dtType);
	void	GetFlexionIndex(int nFingerNum, int nJointNum, std::string &strJointName, DofType &dtType);

private:
	vhtIOConn*		m_pGloveDict;
	vhtCyberGlove*	m_pGlove;
	KinectReader	m_gReader;
	
public:
	int m_actionIdx;								// action index of current frame

private:
	double m_corrThd;								// correlation coefficients low threshold
	double m_corrThd1;								// correlation coefficients low threshold (for gesture "writing!")
	int m_numJudgeFrame;							// number of continuous frames for final action type judgment
	std::vector<int> m_JudgeActionIdx;				// action type index of last "m_numJudgeFrame" frames

	int m_lenFeatureGlove;							// length of Glove feature vector
	double m_gloveFea[20];							// glove feature
	
	int m_numAction;								// number of actions need to be recognized
	double m_corrKinect[11];							// correlation coefficients between current action and template actions (Kinect)
	double m_corrGlove[11];							// correlation coefficients between current action and template actions (Kinect)	

	static double m_TemplateKinect[11][12];			// action template (mean must be subtracted!!!)
	static double m_TemplatePowSumRootKinect[11];	// root of the sum of power value of action templates

	static double m_TemplateGlove[11][20];			// action template (mean must be subtracted!!!)
	static double m_TemplatePowSumRootGlove[11];		// root of the sum of power value of action templates

	static std::string m_ActionTitle[11];			// action title
 
private:
	double FeaMean(double *feaVector, int feaLength);	// mean of feature vector
	void GetActionCorr(DeviceIndex deviceIdx);			// calculate correlation coefficient between current action and action template
	void RecognzieGesture(void);						// action and gesture recognition
	void TemporalSmoothing(int currActionIdx);			// temporal smoothing for action or gesture recognition 
	void ShowActionType(int actionIdx);					// show action and gesture recognition result by text
	void SaveFrame(void);								// save frame data for training

	ProtectedClient<imi::WorldFusionServiceClient> * w_client; //the thrift client

protected:
	
	FusionControlServerArguments* mFusionControlServerArguments;
	ServerToClient* m_serverToClient;
};
