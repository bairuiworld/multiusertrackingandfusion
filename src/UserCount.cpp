#include <iostream>
#include <fstream>
#include <sstream>
#include <string>

#include "UserCount.h"

UserCount::UserCount()
{
}

UserCount::~UserCount()
{
    std::vector<UserDetail*>::iterator it = m_users.begin();
    while(it!=m_users.end())
    {
        delete (*it);
        it++;
    }
}

int UserCount::getNbUsers()
{
    return m_users.size();
}

UserDetail* UserCount::getUserFromId(int idUser)
{
    std::vector<UserDetail*>::iterator it = m_users.begin();
    while(it!=m_users.end() && (*it)->getId()!=idUser)
        it++;
    if(it!=m_users.end())
        return (*it);//&(users_[index])
    return NULL;
}

UserDetail* UserCount::getUserFromIndex(int index)
{
    if(index<0 || index>=(int)m_users.size())
        return NULL;
    return m_users[index];
}

void UserCount::addUser(int idUser)
{
    //new user
    UserDetail* ku = new UserDetail();
	imi::userDetail uD;
    ku->setId(idUser);
    ku->setStatus(UserDetail::NEW);
    m_users.push_back(ku);
	imi::Vec3 vector;
	uD.userId = idUser;
	uD.userName = ku->getName();
	_details.push_back(uD);
}

void UserCount::eraseUser(int idUser)
{
    std::vector<UserDetail*>::iterator it = m_users.begin();
    while(it!=m_users.end() && (*it)->getId()!=idUser)
        it++;
    if(it!=m_users.end())
    {
        delete (*it);
        m_users.erase(it);
    }

	std::vector<imi::userDetail>::iterator itr = _details.begin();
    while(itr!=_details.end() && (itr)->userId!=idUser)
        itr++;
    if(itr!=_details.end())
    {
        //_details.;
        _details.erase(itr);
    }
}
